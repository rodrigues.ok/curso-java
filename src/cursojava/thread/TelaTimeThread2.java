package cursojava.thread;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import javafx.geometry.Dimension2D;

public class TelaTimeThread2 extends JDialog {

	private JPanel jPanel = new JPanel(new GridBagLayout());

	private JLabel descricaoHora = new JLabel("Time da Thread 1");
	private JTextField mostratempo = new JTextField();

	private JLabel descricaoHora2 = new JLabel("Time da Thread 2");
	private JTextField mostratempo2 = new JTextField();

	private JButton jButton = new JButton("Start");
	private JButton jButton2 = new JButton("Stop");

	private Runnable thread1 = new Runnable() {

		@Override
		public void run() {
			while (true) {

				mostratempo.setText(
						new SimpleDateFormat("dd-MM-yyyy  hh:mm:ss ").format(Calendar.getInstance().getTime()));
				try {
					Thread.sleep(1001);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}

		}
	};

	private Runnable thread2 = new Runnable() {

		@Override
		public void run() {
			while (true) {
				mostratempo2
						.setText(new SimpleDateFormat("dd/MM/yyyy hh:mm.ss ").format(Calendar.getInstance().getTime()));

				try {
					Thread.sleep(1001);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	};

	private Thread thead1Time;
	private Thread thead2Time;

/*	public TelaTimeThread() {

		setTitle("Minha Tela de Time com Thread"); 
		setSize(new Dimension(240, 240));
		setLocationRelativeTo(null);
		setResizable(false);*/
		/* Primeira Parte Conclu�da */

	/*	GridBagConstraints gridBangConstraints = new GridBagConstraints();
		gridBangConstraints.gridx = 0;
		gridBangConstraints.gridy = 0;
		gridBangConstraints.gridwidth = 2;
		gridBangConstraints.anchor = gridBangConstraints.WEST;
		gridBangConstraints.insets = new Insets(5, 10, 5, 5);

		descricaoHora.setPreferredSize(new Dimension(200, 25));
		jPanel.add(descricaoHora, gridBangConstraints);

		mostratempo.setPreferredSize(new Dimension(200, 25));
		gridBangConstraints.gridy++;
		mostratempo.setEditable(false);
		jPanel.add(mostratempo, gridBangConstraints);

		descricaoHora2.setPreferredSize(new Dimension(200, 25));
		gridBangConstraints.gridy++;
		jPanel.add(descricaoHora2, gridBangConstraints);

		mostratempo2.setPreferredSize(new Dimension(200, 25));
		mostratempo2.setEditable(false);
		gridBangConstraints.gridy++;
		jPanel.add(mostratempo2, gridBangConstraints);

		gridBangConstraints.gridwidth = 1;

		jButton.setPreferredSize(new Dimension(92, 25));
		gridBangConstraints.gridy++;
		jPanel.add(jButton, gridBangConstraints);

		jButton2.setPreferredSize(new Dimension(92, 25));
		gridBangConstraints.gridx++;
		jPanel.add(jButton2, gridBangConstraints);

		jButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				thead1Time = new Thread(thread1);
				thead1Time.start();

				thead2Time = new Thread(thread2);
				thead2Time.start();

				jButton.setEnabled(false);
				jButton2.setEnabled(true);

			}
		});

		jButton2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				thead1Time.stop();
				thead2Time.stop();

				jButton2.setEnabled(false);
				jButton.setEnabled(true);

			}
		});

		add(jPanel, BorderLayout.WEST);
		/* SetVisible ultimo comando a ser executado */
	/*	setVisible(true);
	} */

}
