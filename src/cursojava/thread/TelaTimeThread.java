package cursojava.thread;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import javafx.geometry.Dimension2D;

public class TelaTimeThread extends JDialog {

	private JPanel jPanel = new JPanel(new GridBagLayout());

	private JLabel descricaoHora = new JLabel("Nome");
	private JTextField mostratempo = new JTextField();

	private JLabel descricaoHora2 = new JLabel("Email");
	private JTextField mostratempo2 = new JTextField();

	private JButton jButton = new JButton("ADD Lista");
	private JButton jButton2 = new JButton("Stop");
	
	
	private ImplementacaoFilathread fila = new ImplementacaoFilathread();

	
	public TelaTimeThread() {

		setTitle("Minha Tela de Time com Thread");
		setSize(new Dimension(240, 240));
		setLocationRelativeTo(null);
		setResizable(false);
		/* Primeira Parte Conclu�da */

		GridBagConstraints gridBangConstraints = new GridBagConstraints();
		gridBangConstraints.gridx = 0;
		gridBangConstraints.gridy = 0;
		gridBangConstraints.gridwidth = 2;
		gridBangConstraints.anchor = gridBangConstraints.WEST;
		gridBangConstraints.insets = new Insets(5, 10, 5, 5);

		descricaoHora.setPreferredSize(new Dimension(200, 25));
		jPanel.add(descricaoHora, gridBangConstraints);

		mostratempo.setPreferredSize(new Dimension(200, 25));
		gridBangConstraints.gridy++;
		
		jPanel.add(mostratempo, gridBangConstraints);

		descricaoHora2.setPreferredSize(new Dimension(200, 25));
		gridBangConstraints.gridy++;
		jPanel.add(descricaoHora2, gridBangConstraints);

		mostratempo2.setPreferredSize(new Dimension(200, 25));
		
		gridBangConstraints.gridy++;
		jPanel.add(mostratempo2, gridBangConstraints);

		gridBangConstraints.gridwidth = 1;

		jButton.setPreferredSize(new Dimension(92, 25));
		gridBangConstraints.gridy++;
		jPanel.add(jButton, gridBangConstraints);

		jButton2.setPreferredSize(new Dimension(92, 25));
		gridBangConstraints.gridx++;
		jPanel.add(jButton2, gridBangConstraints);

		jButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (fila == null){
					fila = new ImplementacaoFilathread();
					fila.start();
					
				}
				
				for(int qtd =0; qtd <100; qtd ++) {
				
				ObjFilaTheard filaTheard = new  ObjFilaTheard();
				filaTheard.setNome(mostratempo.getText());
				filaTheard.setEmail(mostratempo2.getText());
				
				fila.add(filaTheard);
				
				}
				

			}
		});

		jButton2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				fila.stop();
				fila = null;


			}
		});
		

        fila.start();
		add(jPanel, BorderLayout.WEST);
		/* SetVisible ultimo comando a ser executado */
		setVisible(true);
	}

}
