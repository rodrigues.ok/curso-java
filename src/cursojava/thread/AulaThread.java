package cursojava.thread;

import javax.swing.JOptionPane;
import javax.xml.stream.events.StartDocument;

public class AulaThread {

	public static void main(String[] args) throws InterruptedException {

		Thread threadEmail = new Thread(thread1);
		threadEmail.start();

		Thread threadNFCE = new Thread(thread2);
		threadNFCE.start();

		System.out.println("Chegou ao fom do c�digo de teste de thread");

		JOptionPane.showMessageDialog(null, "Thread em backgroud, processamento em paralelo! ");

	}

	private static Runnable thread2 = new Runnable() {

		@Override
		public void run() {
			for (int pos = 0; pos < 10; pos++) {

				System.out.println("Teste de thread a cada 5 segundos - Teste 2 Nota Fiscal");

				try {

					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}
	};

	private static Runnable thread1 = new Runnable() {

		@Override
		public void run() {
			for (int pos = 0; pos < 10; pos++) {

				System.out.println("Teste de thread a cada 3 segundos - Teste 1 Email");

				try {

					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}
	};

}
