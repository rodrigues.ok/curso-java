package cursojava.executavel;

import javax.swing.JOptionPane;

import cursojava.classes.Aluno;
import cursojava.classes.Disciplina;

public class ArrayVetor {
	public static void main(String[] args) {

		double[] notas = { 89.5, 56.7, 87, 6, 85.7 };
		double[] notasLogica = { 93.6, 78.9, 84, 6, 85.8 };

		Aluno aluno = new Aluno();
		aluno.setNome("Rafael");
		aluno.setNomeEscola("Jdev");

		Disciplina disciplina = new Disciplina();
		disciplina.setDisciplina("Java WEB");
		disciplina.setNota(notas);

		aluno.getDisciplinas().add(disciplina);

		Disciplina disciplina2 = new Disciplina();
		disciplina2.setNota(notasLogica);

		aluno.getDisciplinas().add(disciplina2);

		System.out.println("Nome do Aluno: " + aluno.getNome() + " Inscrito no curso: " + aluno.getNomeEscola());
		System.out.println("--------Disciplina do Aluno --------");

		for (Disciplina d : aluno.getDisciplinas()) {

			System.out.println("Disciplina: " + aluno.getDisciplinas());
			System.out.println("As notas da Disciplina s�o: ");

			double notaMax = 0.0;
			for (int pos = 0; pos < d.getNota().length; pos++) {
				System.out.println("Nota " + pos + " � igual = " + d.getNota()[pos]);

				if (notaMax == 0) {

					notaMax = d.getNota()[pos];

				} else {

					if (d.getNota()[pos] > notaMax) {
						notaMax = d.getNota()[pos];
					}
				}

			}
			System.out.println("A maior nota da Disciplina " + d.getDisciplina() + "� de valor = " + notaMax);

		}

	}

}
