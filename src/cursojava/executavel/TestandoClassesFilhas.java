package cursojava.executavel;

import cursojava.classes.Aluno;
import cursojava.classes.Diretor;
import cursojava.classes.Secretario;

public class TestandoClassesFilhas {

	public static void main(String[] args) {

		Aluno aluno = new Aluno();
		aluno.setNome("Rafael");
		aluno.setIdade(21);

		Diretor diretor = new Diretor();
		diretor.setRegistroGeral("112233");
		diretor.setIdade(35);

		Secretario secretario = new Secretario();
		secretario.setExperiencia("Administração");
		secretario.setIdade(35);

		aluno.pessoaMaiorIdade();
		diretor.pessoaMaiorIdade();
		secretario.pessoaMaiorIdade();

		System.out.println(aluno);
		System.out.println(diretor);
		System.out.println(secretario);

		System.out.println(aluno.pessoaMaiorIdade() + " - " + aluno.msgMaiorIdade());
		System.out.println(diretor.pessoaMaiorIdade());
		System.out.println(secretario.pessoaMaiorIdade());
		
		
		System.out.println("Salario = " + diretor.salario());
		System.out.println("Salario = " + aluno.salario());
		System.out.println("Salario = " + secretario.salario());

	}

}
