package cursojava.executavel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

import cursojava.ClasseAuxiliares.FuncaoAutenticacao;
import cursojava.classes.Aluno;
import cursojava.classes.Disciplina;
import cursojava.classes.Secretario;
import cursojava.constantes.StatusAluno;
import cursojava.interfaces.PermitirAcesso;

public class PrimeiraClasse {

	public static void main(String[] args) {

		String login = JOptionPane.showInputDialog("Informe a login");
		String senha = JOptionPane.showInputDialog("Informe a senha");

		try {

			if (new FuncaoAutenticacao(new Secretario(login, senha)).autenticar()) {

				List<Aluno> alunos = new ArrayList<Aluno>();

				HashMap<String, List<Aluno>> maps = new HashMap<String, List<Aluno>>();

				for (int qtd = 1; qtd <= 1; qtd++) {

					String nome = JOptionPane.showInputDialog("Digite o nome do Aluno");
					String idade = JOptionPane.showInputDialog("Digite a idade do Aluno");
					String dataNascimento = JOptionPane.showInputDialog("Digite a data de nascimento do Aluno");
					String rg = JOptionPane.showInputDialog("Digite o RG do Aluno");
					String cpf = JOptionPane.showInputDialog("Digite o CPF do Aluno");
					String nomeMae = JOptionPane.showInputDialog("Digite o nome da M�e do Aluno");
					String nomePai = JOptionPane.showInputDialog("Digite o nome do Pai do Aluno");
					String datamatricula = JOptionPane.showInputDialog("Digite a data da Matricula do Aluno");
					String serie = JOptionPane.showInputDialog("Digite a serie do Aluno");
					String nomeEscola = JOptionPane.showInputDialog("Digite o nome da Escola");

					Aluno aluno1 = new Aluno();

					aluno1.setNome(nome);
					aluno1.setIdade(Integer.valueOf(idade));
					aluno1.setDataNascimento(dataNascimento);
					aluno1.setRegistroGeral(rg);
					aluno1.setNumeroCpf(cpf);
					aluno1.setNomeMae(nomeMae);
					aluno1.setNomePai(nomePai);
					aluno1.setDataMatricula(datamatricula);
					aluno1.setSerieMatriculado(serie);
					aluno1.setNomeEscola(nomeEscola);

					for (int pos = 1; pos <= 2; pos++) {

						String nomeDisciplina = JOptionPane.showInputDialog("Digite o nome da Disciplina" + pos + "?");
						String notaDisciplina = JOptionPane.showInputDialog("Digite o valor da Nota" + pos + "?");

						Disciplina disciplina = new Disciplina();
						disciplina.setDisciplina(nomeDisciplina);
				/*		disciplina.setNota(Double.valueOf(notaDisciplina));*/
						aluno1.getDisciplinas().add(disciplina);

					}

					int escolha = JOptionPane.showConfirmDialog(null, "Deseja remover alguma Disciplina?");

					if (escolha == 0) {

						int continuarRemover = 0;
						int posicao = 1;

						while (continuarRemover == 0) {

							String disciplinaRemover = JOptionPane
									.showInputDialog("Qual disciplina ser� removida 1 ou 2?");
							aluno1.getDisciplinas().remove(Integer.valueOf(disciplinaRemover).intValue() - posicao);
							posicao++;
							continuarRemover = JOptionPane.showConfirmDialog(null, "Continuar a Remover?");

						}
					}

					alunos.add(aluno1);

				}

				maps.put(StatusAluno.APROVADO, new ArrayList<Aluno>());
				maps.put(StatusAluno.REPROVADO, new ArrayList<Aluno>());
				maps.put(StatusAluno.RECUPERACAO, new ArrayList<Aluno>());

				for (Aluno aluno : alunos) {

					if (aluno.getAlunoAprovado2().equalsIgnoreCase(StatusAluno.APROVADO)) {
						maps.get(StatusAluno.APROVADO).add(aluno);

					} else if (aluno.getAlunoAprovado2().equalsIgnoreCase(StatusAluno.RECUPERACAO)) {
						maps.get(StatusAluno.RECUPERACAO).add(aluno);

					} else if (aluno.getAlunoAprovado2().equalsIgnoreCase(StatusAluno.REPROVADO)) {
						maps.get(StatusAluno.REPROVADO).add(aluno);
					}

				}

				System.out.println("---------------LISTA DOS APROVADOS ----------------------");
				for (Aluno aluno : maps.get(StatusAluno.APROVADO)) {

					System.out.println(
							"Resultado = " + aluno.getAlunoAprovado2() + "com m�dia de  = " + aluno.getMediaNota());
				}

				System.out.println("---------------LISTA EM RECUPERA��O ----------------------");
				for (Aluno aluno : maps.get(StatusAluno.RECUPERACAO)) {
					System.out.println(
							"Resultado = " + aluno.getAlunoAprovado2() + "com m�dia de = " + aluno.getMediaNota());

				}
				System.out.println("---------------LISTA DOS REPROVADOS ----------------------");

				for (Aluno aluno : maps.get(StatusAluno.REPROVADO)) {

					System.out.println(
							"Resultado = " + aluno.getAlunoAprovado2() + "com m�dia de = " + aluno.getMediaNota());

				}

			} else
				JOptionPane.showMessageDialog(null, "Acesso n�o permitido");
			System.exit(0);
		} catch (Exception e) {
			e.printStackTrace();

			JOptionPane.showMessageDialog(null, "Erro Inesperado : " + e.getClass().getName());

		} finally {

			JOptionPane.showMessageDialog(null, "Obrigado por estudar java!!");

		}

	}
}
