package cursojava.executavel;

import cursojava.classes.Aluno;
import cursojava.classes.Disciplina;

public class ArrayVetor2 {
	public static void main(String[] args) {

		double[] notas = { 89.5, 56.7, 87, 6, 85.7 };
		double[] notasLogica = { 93.6, 78.9, 84, 6, 85.8 };

		Aluno aluno = new Aluno();
		aluno.setNome(" Rafael");
		aluno.setNomeEscola("Jdev");

		Disciplina disciplina = new Disciplina();
		disciplina.setDisciplina("Java WEB");
		disciplina.setNota(notas);

		aluno.getDisciplinas().add(disciplina);

		Disciplina disciplina2 = new Disciplina();
		disciplina2.setDisciplina("Java");
		disciplina2.setNota(notasLogica);

		aluno.getDisciplinas().add(disciplina2);
		

		System.out.println("**************************************************************");

		Aluno[] arrayAlunos = new Aluno[1];
		arrayAlunos[0] = aluno;
		for (int pos = 0; pos < arrayAlunos.length; pos++) {

			System.out.println("Nome do ALuno �: " + arrayAlunos[pos].getNome());

			for (Disciplina d : arrayAlunos[pos].getDisciplinas()) {

				System.out.println("Nome da Disciplina: " + d.getDisciplina());
				
				for(int posnota =0; posnota<d.getNota().length;posnota ++) {
					
					System.out.println(" A nota n�mero: "+ posnota + "� igual = " +d.getNota()[posnota]);
					
				}
			}

		}

	}

}
