package cursojava.executavel;

import java.util.Arrays;
import java.util.List;

public class SplitArray {
	public static void main(String[] args) {

		String texto = "Rafael,Curso java,80,70,90,89";

		String[] valoresArray = texto.split(",");
		System.out.println("Nome: " + valoresArray[0]);
		System.out.println("Nome: " + valoresArray[1]);
		System.out.println("Nome: " + valoresArray[2]);
		System.out.println("Nome: " + valoresArray[3]);
		System.out.println("Nome: " + valoresArray[4]);
		System.out.println("Nome: " + valoresArray[5]);

		System.out.println("========================================");

		/* Converter Array em uma Lista */

		List<String> list = Arrays.asList(valoresArray);

		for (String valorString : list) {

			System.out.println(valorString);
		}

		System.out.println("========================================");

		/* Converter uma Lista para um Array */

		String[] conversaoArray = list.toArray(new String[6]);
		System.out.println(conversaoArray);

		for (int pos = 0; pos < conversaoArray.length; pos++) {
			System.out.println(conversaoArray[pos]);

		}

	}
}
